Comandos Maven
Comandos para utilizar em projeto

mostra a arvore de dependencies(.jar)

mvn depedency:tree

copia os jar(dependencies) para pasta target/dependency [ evita eventuais problemas de ambiente ]

mvn dependency:copy-dependencies

compile o projeto

mvn compile

executa os testes

mvn test

gerar os .jars , muito usado em projetos ear

mvn package

limpa todas as dependencies(.jars)

mvn clean

procura todos os comandos que vc deu para o maven

history | grep mvn

boa prática adotada para gerar o pacote de deploy do projeto

mvn clean install