package com.api.pdf.pdf.defaults;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.Barcode128;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.IOException;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */
public class Barcodes {
    PdfWriter writer;
    
    /**
     * @param writer 
     */
    public Barcodes(PdfWriter writer) {
        this.writer = writer;
    }
    
    /**
     * @param textCode
     * @return PdfPTable
     * @throws IOException
     * @throws DocumentException 
     */
    public PdfPTable getLayout(String textCode) throws IOException, DocumentException {
        PdfPTable table = new PdfPTable(1);
        table.getDefaultCell().setBorder(0);
        table.setWidthPercentage(26.5f);
        table.setHorizontalAlignment(Rectangle.ALIGN_RIGHT);
        table.addCell(barcode(textCode));
        
        return table;
    }
    
    /**
     * @param text
     * @return Image
     * @throws IOException 
     */
    private PdfPCell barcode(String text) throws IOException {
        Barcode128 barcode = new Barcode128();
        barcode.setCodeType(Barcode128.CODE128);
        barcode.setSize(8.2f);
        barcode.setCode(text);
        barcode.setCodeType(Barcode128.CODE128);
        barcode.setTextAlignment(Rectangle.ALIGN_RIGHT);
        barcode.setBaseline(-3);
        barcode.setChecksumText(true);
        barcode.setBarHeight(28);
       
        PdfPCell cell = new PdfPCell();
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setPaddingTop(-40);
        cell.addElement(barcode.createImageWithBarcode(writer.getDirectContent(), null, null));
        cell.setPaddingRight(70);
        
        return cell;
    }
    
    public Barcode128 barcodeHeader(String text) {
        Barcode128 barcode = new Barcode128();
        barcode.setCodeType(Barcode128.CODE128);
        barcode.setSize(8.2f);
        barcode.setCode(text);
        barcode.setCodeType(Barcode128.CODE128);
        barcode.setTextAlignment(Rectangle.ALIGN_RIGHT);
        barcode.setBaseline(-3);
        barcode.setChecksumText(true);
        barcode.setBarHeight(28);
        
        return barcode;
    }
}
