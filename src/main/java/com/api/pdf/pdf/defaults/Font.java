package com.api.pdf.pdf.defaults;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Phrase;
import java.io.IOException;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */
public class Font {
    public static com.itextpdf.text.Font bold() throws DocumentException, IOException {
        return new com.itextpdf.text.Font(com.itextpdf.text.Font.FontFamily.HELVETICA, 11, com.itextpdf.text.Font.BOLD);
    }
    
    public static com.itextpdf.text.Font normal() {
        return new com.itextpdf.text.Font(com.itextpdf.text.Font.FontFamily.HELVETICA, 11, com.itextpdf.text.Font.NORMAL);
    }
    
    /**
     * @param phrase
     * @param fontSize
     * @return Phrase
     * @throws DocumentException
     * @throws IOException 
     */
    public static Phrase phraseBold(String phrase, int fontSize) throws DocumentException, IOException {
        com.itextpdf.text.Font fontBold = Font.bold();
        fontBold.setSize(fontSize);
        
        Phrase p = new Phrase();
        p.add(new Chunk(phrase, fontBold));
        
        return p;
    }
    
    /**
     * @param phrase
     * @param fontSize
     * @return Phrase
     */
    public static Phrase phraseNormal(String phrase, int fontSize) {
        com.itextpdf.text.Font fontNormal = Font.normal();
        fontNormal.setSize(fontSize);
        
        Phrase p = new Phrase();
        p.add(new Chunk(phrase, fontNormal));
        
        return p;
    }
    
    /**
     * @param phrase
     * @param fontSize
     * @return Phrase
     * @throws DocumentException
     * @throws IOException 
     */
    public static Phrase phraseBoldUnderline(String phrase, int fontSize) throws DocumentException, IOException {
        com.itextpdf.text.Font fontBold = Font.bold();
        fontBold.setSize(fontSize);
        
        Phrase p = new Phrase();
        p.add(new Chunk(phrase, fontBold).setUnderline(1.2f, -2.2f));
        
        return p;
    }
}
