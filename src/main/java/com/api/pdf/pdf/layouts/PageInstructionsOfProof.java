package com.api.pdf.pdf.layouts;

import com.api.pdf.pdf.defaults.Font;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import java.io.IOException;
import org.bson.Document;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */
public class PageInstructionsOfProof {
    /**
     * @param document
     * @return PdfPTable
     * @throws java.io.IOException
     * @throws com.itextpdf.text.DocumentException
     */
    public PdfPTable getLayout(Document document) throws IOException, DocumentException {
        PdfPTable table = new PdfPTable(1);
        table.getDefaultCell().setBorder(0);
        
        table.addCell(this.title(String.format("PROVA MULTIDISCIPLINAR (%d-%d)", document.getInteger("year"), document.getInteger("period"))));
        table.addCell(this.candidateInfo(document));
        table.addCell(this.signature(document));
        table.addCell(this.title("INSTRUÇÕES IMPORTANTES"));
        table.addCell(this.instructions());
        table.addCell(this.title("GABARITO"));
        table.addCell(this.template());
        
        return table;
    }
    
    /**
     * @param title
     * @return PdfPTable
     * @throws DocumentException
     * @throws IOException 
     */
    private PdfPTable title(String title) throws DocumentException, IOException {
        com.itextpdf.text.Font font = Font.bold();
        font.setColor(BaseColor.WHITE);
        font.setSize(16);
        
        Phrase p = new Phrase();
        p.add(new Chunk(title, font));
        
        PdfPCell cell = new PdfPCell(p);
        cell.setBackgroundColor(BaseColor.DARK_GRAY);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setPaddingTop(8);
        cell.setPaddingBottom(10);
        
        PdfPTable table = new PdfPTable(1);
        table.addCell(cell);
        
        return table;
    }
    
    /**
     * @param title
     * @param colspan
     * @param rowspan
     * @param paddingTop
     * @param paddingBottom
     * @return PdfPCell
     * @throws DocumentException
     * @throws IOException 
     */
    private static PdfPCell cell(Phrase title, int colspan, int rowspan, int paddingTop, int paddingBottom) throws DocumentException, IOException {
        PdfPCell cell = new PdfPCell(title);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setColspan(colspan);
        cell.setRowspan(rowspan);
        cell.setPaddingTop(paddingTop);
        cell.setPaddingBottom(paddingBottom);
        
        return cell;
    }
    
    private PdfPTable candidateInfo(Document document) throws IOException, DocumentException {
        Document course = (Document) document.get("course");
        Document testSite = (Document) document.get("testSite");
        PdfPTable table = new PdfPTable(5);
        
        table.addCell(cell(Font.phraseBoldUnderline("Aluno", 13), 2, 1, 10, 5));
        table.addCell(cell(Font.phraseBoldUnderline("Matrícula", 13), 1, 1, 10, 5));
        table.addCell(cell(Font.phraseBoldUnderline("Curso", 13), 2, 1, 10, 5));
        
        table.addCell(cell(Font.phraseNormal(document.getString("student"), 11), 2, 1, 0, 0));
        table.addCell(cell(Font.phraseNormal(document.getInteger("enrollment").toString(), 11), 1, 1, 0, 0));
        table.addCell(cell(Font.phraseNormal(String.format("%s - %s", course.getString("name"), course.getString("initials")), 11), 2, 1, 0, 0));
        
        table.addCell(cell(Font.phraseBoldUnderline("Documento", 13), 2, 1, 10, 5));
        table.addCell(cell(Font.phraseBoldUnderline("Turno", 13), 1, 1, 10, 5));
        table.addCell(cell(Font.phraseBoldUnderline("Sala", 13), 2, 1, 10, 5));
        
        table.addCell(cell(Font.phraseNormal(document.getString("identity"), 11), 2, 1, 0, 0));
        table.addCell(cell(Font.phraseNormal(course.getString("shift"), 11), 1, 1, 0, 0));
        table.addCell(cell(Font.phraseNormal(testSite.getInteger("room").toString(), 11), 2, 1, 0, 10));
        
        return table;
    }
    
    private PdfPTable instructions() throws DocumentException, IOException {
        PdfPTable table = new PdfPTable(2);
        Phrase phrase;
        PdfPCell addCell;
        
        com.itextpdf.text.Font fontNormal = Font.normal();
        fontNormal.setSize(9);
        
        com.itextpdf.text.Font fontBold = Font.bold();
        fontBold.setSize(9);
        
        phrase = Font.phraseNormal(
            "01 - Para cada um das questões objetivas, são apresentadas 4 alternativas classificadas com as letras (A), (B), (C) e (D);" + 
            "só uma responde adequadamente ao quesito proposto. Você só deve assinar UMA RESPOSTA. " + 
            "A marcação em mais de uma alternativa anula a questão, MESMO QUE UMA DAS RESPOSTAS ESTEJA CORRETA\n\n", 
            9
        );
        addCell = cell(phrase, 1, 1, 0, 0);
        addCell.setPaddingRight(10);
        addCell.setPaddingTop(10);
        table.addCell(addCell).setHorizontalAlignment(Rectangle.ALIGN_JUSTIFIED);
        
        phrase = Font.phraseNormal(
            "03 - Tenha muito cuidado com o CARTÃO-RESPOSTA para não DOBRAR, AMASSAR ou MANCHAR. O mesmo poderá ser substituido se, no ato da entrega " + 
            "ao candidato, já estiver danificado em suas margens superior e/ou inferior.\n\n", 
            9
        );
        addCell = cell(phrase, 1, 1, 0, 0);
        addCell.setPaddingRight(10);
        addCell.setPaddingTop(10);
        table.addCell(addCell).setHorizontalAlignment(Rectangle.ALIGN_JUSTIFIED);
       
        phrase = new Phrase();
        phrase.add(new Chunk("02 - SERÁ ATRIBUIDA NOTA 0 (ZERO) o aluno que:\n", fontBold));
        phrase.add(new Chunk("A) Se utilizar, durante a realização das provas qualquer tipo de fontes para consultas;\n", fontNormal));
        phrase.add(new Chunk("B) Se ausentar da sala em que se realizam as provas levando consigo o CADERNO DE QUESTÕES e/ou CARTÃO-RESPOSTA;\n", fontNormal));
        phrase.add(new Chunk("C) Se recusar a entregar o CADERNO DE QUESTÕES e/ou o CARTÃO-RESPOSTA, quando terminar o tempo estabelecido;\n", fontNormal));
        phrase.add(new Chunk("D) Se não assinar a LISTA DE PRESENÇA e o CARTÃO-RESPOSTA\n\n", fontNormal));
        phrase.add(new Chunk("OBS: \n", fontBold));
        phrase.add(new Chunk(
            "O candidato só poderá se ausentar do recinto das provas após 1 (uma) hora contada a partir do efetivo início das mesmas. " +
            "Por motivos de segurança, o candidato NÃO PODERÁ LEVAR O CARDERNO DE QUESTÕES. " + 
            "A prova estará disponível para o aluno no seu sistema acadêmico juntamente com seu gabarito até (2) dias após a realização da prova.", fontNormal)
        );
        addCell = cell(phrase, 1, 1, 0, 0);
        addCell.setPaddingRight(10);
        table.addCell(addCell).setHorizontalAlignment(Rectangle.ALIGN_JUSTIFIED);
        
        phrase = new Phrase();
        phrase.add(new Chunk("04 - Verifique se este material está em ordem e se o seu nome confere com que aparece no cartão resposta. Caso contrário, notifique o fato IMEDIATAMENTE ao fiscal.\n\n", fontNormal));
        phrase.add(new Chunk("05 - Após a conferência, o candidato deverá assinar, no espaço próprio às respostas certas.\n\n", fontNormal));
        phrase.add(new Chunk("06 - As questões objetivas são identificadas pelo número que se situa acima do seu enumerado.\n\n", fontNormal));
        phrase.add(new Chunk("07 - Reserve os 30 (trinta) minutos finais para marcar seu CARTÃO-RESPOSTA. Os rascunhos e as marcações assinaladas no CADERNO DE QUESTÕES NÃO SERÃO LEVADAS EM CONTA.\n\n", fontNormal));
        phrase.add(new Chunk("08 - O tempo disponível para realização da prova é de 3 HORAS e 30 MINUTOS, ou seja, DAS 08:00 às 11:30 para alunos da manhã e tarde e 19:00 às 22:30 para alunos da noite.", fontNormal));
        table.addCell(cell(phrase, 1, 1, 0, 0)).setHorizontalAlignment(Rectangle.ALIGN_JUSTIFIED);
        
        PdfPCell cell = new PdfPCell();
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setColspan(3);
        cell.setFixedHeight(10);
        table.addCell(cell);
        
        return table;
    }
    
    private PdfPTable signature(Document document) throws DocumentException, IOException {
        com.itextpdf.text.Font fontNormal = Font.normal();
        fontNormal.setSize(9);
        
        com.itextpdf.text.Font fontBold = Font.bold();
        fontBold.setSize(14);
        
        PdfPTable table = new PdfPTable(3);
        PdfPCell cell;
        Paragraph p;
        
        p = new Paragraph();
        p.setAlignment(Element.ALIGN_CENTER);
        p.setFont(fontBold);
        p.add(new Chunk("ASSINATURA"));
        
        cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setColspan(1);
        cell.setPaddingTop(25);
        cell.setPaddingBottom(25);
        cell.setPaddingRight(20);
        cell.setPaddingLeft(20);
        cell.addElement(p);
        
        table.addCell(cell);
        
        p = new Paragraph();
        p.setAlignment(Element.ALIGN_CENTER);
        p.add(new Chunk("______________________________________\n"));
        p.add(new Chunk(document.getString("student"), fontNormal));
        
        cell = new PdfPCell();
        cell.setColspan(2);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.addElement(p);
        
        table.addCell(cell);
        
        cell = new PdfPCell();
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setColspan(3);
        cell.setFixedHeight(10);
        table.addCell(cell);
        
        return table;
    }
    
    public PdfPTable template() throws DocumentException, IOException {
        com.itextpdf.text.Font fontNormal = Font.normal();
        fontNormal.setSize(9);
        
        com.itextpdf.text.Font fontBold = Font.bold();
        fontBold.setSize(9);
        
        PdfPTable table = new PdfPTable(1);
        PdfPCell cell;
        Phrase phrase;
        Image image;
        
        phrase = new Phrase();
        phrase.add(new Chunk("ATENÇÃO: \n", fontBold));
        phrase.add(new Chunk("No CARTÃO-RESPOSTA, a marcação das letras correspondentes às respostas certas deve ser feita ", fontNormal));
        phrase.add(new Chunk("COBRINDO A LETRA E PREENCHENDO TODO O ESPAÇO COMPREENDIDO PELAS LETRAS, a caneta esferográfica transparente de tinta PRETA ou AZUL, ", fontBold));
        phrase.add(new Chunk("de forma contínua e densa, conforme o exemplo abaixo: ", fontNormal));
        
        cell = new PdfPCell();
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
        cell.addElement(phrase);
        cell.setHorizontalAlignment(Rectangle.ALIGN_JUSTIFIED);
        table.addCell(cell);
        
        image = Image.getInstance(getClass().getResource("/gabarito.jpg"));
        image.setBorder(Rectangle.NO_BORDER);
        image.scalePercent(25);
        
        cell = new PdfPCell(image);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(120);
        cell.setPaddingTop(20);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);
        
        return table;
    }
}
