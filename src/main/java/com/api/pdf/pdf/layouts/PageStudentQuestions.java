package com.api.pdf.pdf.layouts;


import com.api.pdf.pdf.defaults.Font;
import com.itextpdf.text.*;
import com.itextpdf.text.html.simpleparser.HTMLWorker;
import com.itextpdf.text.html.simpleparser.StyleSheet;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.tool.xml.exceptions.RuntimeWorkerException;
import org.apache.commons.lang3.StringUtils;
import org.bson.Document;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */
public class PageStudentQuestions {
    public static final String CSS = "p { text-align: justify; font-size:14px; }";

    /**
     * @param document
     * @return
     * @throws IOException
     * @throws DocumentException
     */
    public PdfPTable getLayout(Document document) throws IOException, DocumentException {
        List<Document> disciplines = (List<Document>) document.get("disciplines");
        PdfPTable table = new PdfPTable(1);

        for (Document discipline : disciplines) {
            try {
                List<Document> questions = (List<Document>) discipline.get("questions");

                PdfPCell separatorBorderCell = new PdfPCell();
                separatorBorderCell.setBorder(Rectangle.NO_BORDER);
                separatorBorderCell.setFixedHeight(5);

                com.itextpdf.text.Font font = Font.bold();
                font.setColor(BaseColor.BLACK);
                font.setSize(13);

                Phrase p = new Phrase();
                p.add(new Chunk(String.format("%s (%s)", discipline.getString("name"), discipline.getString("initials")), font));

                PdfPCell cellDiscipline = new PdfPCell(p);
                cellDiscipline.setBorderWidthLeft(Rectangle.NO_BORDER);
                cellDiscipline.setBorderWidthTop(Rectangle.NO_BORDER);
                cellDiscipline.setBorderWidthRight(Rectangle.NO_BORDER);
                cellDiscipline.setBorderColorBottom(BaseColor.DARK_GRAY);
                cellDiscipline.setBorderWidthBottom(2);
                cellDiscipline.setBackgroundColor(BaseColor.LIGHT_GRAY);
                cellDiscipline.setHorizontalAlignment(Element.ALIGN_CENTER);
                cellDiscipline.setPaddingTop(4);
                cellDiscipline.setPaddingBottom(8);

                table.addCell(separatorBorderCell);
                table.addCell(cellDiscipline);

                for (Document question : questions) {
                    try {

                        List<Document> answers = (List<Document>) question.get("answers");

                        table.addCell(this.questionTitle(
                                question.getString("code"),
                                question.getInteger("number"),
                                question.getString("name"),
                                question.getString("source"),
                                document.getInteger("enrollment"),
                                answers
                        ));
                    } catch (IOException ex) {
                        System.out.println("IO Exception: " + ex);
                    } catch (DocumentException ex) {
                        System.out.println("Document Exception: " + ex);
                    }
                }
            } catch (IOException ex) {
                System.out.println("IO Exception: " + ex);
            } catch (DocumentException ex) {
                System.out.println("Document Exception: " + ex);
            }
        }

        return table;
    }

    /**
     * @param code
     * @param number
     * @param question
     * @param source
     * @param enrollment
     * @param answers
     * @return
     * @throws IOException
     * @throws DocumentException
     */
    private PdfPCell questionTitle(String code, int number, String question, String source, int enrollment, List<Document> answers) throws IOException, DocumentException {
        boolean sourceIsNotEmpty = StringUtils.isNotEmpty(source);

        question.replace("<br data-mce-bogus=\\\"1\\\">", "<br />");
        if (question.endsWith("</p>") && !question.startsWith("<p>")) {
            question = "<p>" + question;
        }

        question = "<p>" + question + "</p>";

        PdfPCell cellQuestion = new PdfPCell();
        cellQuestion.setBorder(Rectangle.NO_BORDER);
        cellQuestion.setPaddingBottom(10);

        try {
            Map<String, String> mapCss = new HashMap<>();
            mapCss.put("size", "10pt");
            mapCss.put("align", "justify");

            StyleSheet styles = new StyleSheet();
            styles.loadTagStyle("p", mapCss);

            List<Element> list = new ArrayList<>();
            list = HTMLWorker.parseToList(new StringReader(question), styles);

            for (Element element : list) {
                cellQuestion.addElement(element);
            }

        } catch (RuntimeWorkerException ex) {
            System.out.println("Doc: " + enrollment + " - Error: " + ex);
        }

        PdfPCell cellTitle = new PdfPCell();
        cellTitle.setBorder(Rectangle.NO_BORDER);
        cellTitle.setPaddingBottom(sourceIsNotEmpty ? 0 : 10);
        cellTitle.addElement(Font.phraseBoldUnderline(String.format("Questão %d (código: %s)", number, code), 12));

        PdfPTable table = new PdfPTable(1);
        table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
        table.setWidthPercentage(100);
        table.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
        table.addCell(cellTitle);

        if (sourceIsNotEmpty) {
            Paragraph paragraph = new Paragraph();
            paragraph.add(Font.phraseBold("Fonte: ", 10));
            paragraph.add(Font.phraseNormal(source, 10));

            PdfPCell cellSource = new PdfPCell();
            cellSource.setBorder(Rectangle.NO_BORDER);
            cellSource.setPaddingBottom(10);
            cellSource.addElement(paragraph);
            table.addCell(cellSource);
        }

        PdfPCell separatorHeightCell = new PdfPCell();
        separatorHeightCell.setBorder(Rectangle.NO_BORDER);
        separatorHeightCell.setFixedHeight(20);

        PdfPCell separatorBorderCell = new PdfPCell();
        separatorBorderCell.setBorder(Rectangle.BOX);
        separatorBorderCell.setBorderWidthLeft(Rectangle.NO_BORDER);
        separatorBorderCell.setBorderWidthRight(Rectangle.NO_BORDER);
        separatorBorderCell.setPaddingTop(15);
        separatorBorderCell.setFixedHeight(3);

        PdfPCell borderCellSecond = new PdfPCell();
        borderCellSecond.setBorder(Rectangle.TOP);
        borderCellSecond.setBorderColorBottom(new BaseColor(100, 100, 100));

        table.addCell(cellQuestion);

        for (Document answer : answers) {
            try {
                table.addCell(this.answerQuestion(answer.getString("alternative"), answer.getString("answer"), enrollment));
            } catch (IOException ex) {
                System.out.println("IO Exception: " + ex);
            } catch (DocumentException ex) {
                System.out.println("Document Exception: " + ex);
            }
        }

        table.addCell(separatorHeightCell);
        table.addCell(separatorBorderCell);

        PdfPCell cell = new PdfPCell();
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setPaddingTop(10);
        cell.addElement(table);

        return cell;
    }

    /**
     * @param alternative
     * @param answer
     * @return PdfPCell
     * @throws IOException
     * @throws DocumentException
     */
    private PdfPCell answerQuestion(String alternative, String answer, int enrollment) throws IOException, DocumentException {
        answer.replace("<br data-mce-bogus=\\\"1\\\">", "<br />");
        if (answer.endsWith("</p>") && !answer.startsWith("<p>")) {
            answer = "<p>" + answer;
        }

        answer = "<p>" + answer + "</p>";

        Phrase p = new Phrase();
        p.add(Font.phraseBold(String.format("%s) ", alternative), 10));

        try {
            Map<String, String> mapCss = new HashMap();
            mapCss.put("size", "10pt");

            StyleSheet styles = new StyleSheet();
            styles.loadTagStyle("p", mapCss);

            List<Element> list = new ArrayList();
            list = HTMLWorker.parseToList(new StringReader(answer), styles);

            for (Element element : list) {
                p.add(element);
            }
        } catch (RuntimeWorkerException ex) {
            System.out.println("Doc: " + enrollment + " - Error: " + ex);
        }

        PdfPCell cell = new PdfPCell();
        cell.setBorder(Rectangle.NO_BORDER);
        cell.addElement(p);

        return cell;
    }
}
