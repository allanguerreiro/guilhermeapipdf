package com.api.pdf.pdf;

import com.api.pdf.pdf.defaults.Barcodes;
import com.api.pdf.pdf.eventhandler.HeaderFooterPageEvent;
import com.api.pdf.pdf.layouts.PageInstructionsOfProof;
import com.api.pdf.pdf.layouts.PageStudentQuestions;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.Barcode128;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */
public class GeneratePdfDocument {
    /**
     * @param filename
     * @param documentBson
     * @throws IOException
     * @throws DocumentException 
     */
    public GeneratePdfDocument(String filename, org.bson.Document documentBson) throws IOException, DocumentException {
        Document documentPdf = new Document();
        PdfWriter writer = PdfWriter.getInstance(documentPdf, new FileOutputStream(filename));
        
        documentPdf.addAuthor("Desenvolvido e mantido pelo Setor de Tecnologia da Informação - Univiçosa");
        documentPdf.addTitle(String.format("%d - %s", documentBson.getInteger("enrollment"), documentBson.getString("student")));
        documentPdf.addCreator("Univiçosa");
        documentPdf.addLanguage("pt-BR");
        documentPdf.setMargins(-45, -45, 65, 35);
        documentPdf.setPageSize(PageSize.A4);
        documentPdf.open();
       
        Barcodes barcodes = new Barcodes(writer);
        PageInstructionsOfProof pageInstructions = new PageInstructionsOfProof();
        PageStudentQuestions pageStudentQuestions = new PageStudentQuestions();
        
        Barcode128 imageBarcode = barcodes.barcodeHeader(String.format("MD%d%d%d", documentBson.getInteger("year"), documentBson.getInteger("period"), documentBson.getInteger("enrollment")));
        HeaderFooterPageEvent event = new HeaderFooterPageEvent(imageBarcode);
        writer.setPageEvent(event);
        
        documentPdf.add(barcodes.getLayout(String.format("MD%d%d%d", documentBson.getInteger("year"), documentBson.getInteger("period"), documentBson.getInteger("enrollment"))));
        documentPdf.add(pageInstructions.getLayout(documentBson));
        documentPdf.newPage();
        documentPdf.add(pageStudentQuestions.getLayout(documentBson));
        
        documentPdf.close();
    }
}
