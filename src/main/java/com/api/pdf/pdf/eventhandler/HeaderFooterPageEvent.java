package com.api.pdf.pdf.eventhandler;

import com.api.pdf.pdf.defaults.Font;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */
public class HeaderFooterPageEvent extends PdfPageEventHelper {
    private Barcode128 barcode;

    /**
     * @param barcode
     */
    public HeaderFooterPageEvent(Barcode128 barcode) {
        this.barcode = barcode;
    }

    @Override
    public void onStartPage(PdfWriter writer, Document document) {
        PdfContentByte canvas = writer.getDirectContent();
        PdfTemplate template = this.barcode.createTemplateWithBarcode(canvas, BaseColor.BLACK, BaseColor.BLACK);

        float x = document.getPageNumber() % 2 == 0 ? 25 : 465;
        float y = 785;
        float w = template.getWidth();
        float h = template.getHeight();

        canvas.saveState();
        canvas.setColorFill(BaseColor.WHITE);
        canvas.rectangle(x, y, w, h);
        canvas.fill();
        canvas.restoreState();
        canvas.addTemplate(template, x, y);

        ColumnText.showTextAligned(canvas, Rectangle.ALIGN_RIGHT, null, 0, 0, 0);
    }

    @Override
    public void onEndPage(PdfWriter writer, Document document) {
        try {
            int pageNumber = document.getPageNumber();

            if (pageNumber == 1) {
                ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER, Font.phraseBold("Univiçosa - Faculdade de Ciências e Tecnologia de Viçosa", 9), 150, 20, 0);

                Image image = Image.getInstance(getClass().getResource("/logo.jpg"));
                image.setBorder(Rectangle.NO_BORDER);
                image.scalePercent(15);

                PdfPCell cell = new PdfPCell(image);
                cell.setBorder(Rectangle.NO_BORDER);

                PdfPTable table = new PdfPTable(1);
                table.setTotalWidth(15);
                table.addCell(cell);
                table.writeSelectedRows(0, -1, 480, 40, writer.getDirectContent());
            }

            if (pageNumber > 1) {
                com.itextpdf.text.Font fontBold = Font.bold();
                fontBold.setSize(11);
                fontBold.setColor(BaseColor.WHITE);

                Chunk chunk = new Chunk(String.format("%d", pageNumber - 1), fontBold);
                chunk.setBackground(new BaseColor(100, 100, 100), 8, 5, 8, 8);

                Phrase p = new Phrase();
                p.add(chunk);

                ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER, p, pageNumber % 2 == 0 ? 40 : 560, 20, 0);
            }
        } catch (IOException ex) {
            Logger.getLogger(HeaderFooterPageEvent.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DocumentException dex) {
            Logger.getLogger(HeaderFooterPageEvent.class.getName()).log(Level.SEVERE, null, dex);
        }
    }
}
