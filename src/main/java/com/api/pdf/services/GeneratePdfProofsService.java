package com.api.pdf.services;

import com.api.pdf.db.collections.FindAllStudentsProofs;
import com.api.pdf.pdf.GeneratePdfDocument;
import com.api.pdf.utils.CreateDirectories;
import com.api.pdf.utils.CreatedDocuments;
import com.api.pdf.utils.FailedDocuments;
import com.api.pdf.utils.PdfReportList;
import com.google.gson.Gson;
import com.itextpdf.text.DocumentException;
import org.bson.Document;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */

@Path("/md")
public class GeneratePdfProofsService {
    private int count = 0;
    private List<CreatedDocuments> created;
    private List<FailedDocuments> failed;

    public GeneratePdfProofsService() {
        this.created = new ArrayList();
        this.failed = new ArrayList();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String generate(@QueryParam("enrollment") int enrollment) throws DocumentException {
        Gson gson = new Gson();
        List<Document> proofs = new FindAllStudentsProofs().getDocuments(enrollment);

        for (Document document : proofs) {
            String filename = String.format("%d.pdf", document.getInteger("enrollment"));
            CreateDirectories createDirectories = new CreateDirectories(document);
            File basePath = createDirectories.create();

            try {
                GeneratePdfDocument generatePdfDocument = new GeneratePdfDocument(basePath.toString() + "/" + filename, document);
            } catch (IOException ex) {
                this.failed.add(new FailedDocuments(basePath.toPath().toString(), String.format("%s.pdf", document.getInteger("enrollment").toString())));
                System.out.println(String.format("IOException: %d - Error: %s", document.getInteger("enrollment"), ex));
            } catch (DocumentException dex) {
                this.failed.add(new FailedDocuments(basePath.toPath().toString(), String.format("%s.pdf", document.getInteger("enrollment").toString())));
                System.out.println(String.format("Doc: %d - Error: %s", document.getInteger("enrollment"), dex));
            }

            this.count++;
            this.created.add(new CreatedDocuments(basePath.toPath().toString(), String.format("%s.pdf", document.getInteger("enrollment").toString())));
        }

        PdfReportList report = new PdfReportList(this.created, this.failed, this.count);
        return gson.toJson(report);
    }
}
