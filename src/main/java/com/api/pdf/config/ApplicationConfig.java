package com.api.pdf.config;

import com.api.pdf.services.GeneratePdfProofsService;

import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */
public class ApplicationConfig extends Application {
    @Override
    public Set<Class<?>> getClasses() {
        final Set<Class<?>> classes = new HashSet();

        classes.add(GeneratePdfProofsService.class);
        return classes;
    }
}
