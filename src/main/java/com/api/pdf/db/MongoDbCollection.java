package com.api.pdf.db;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */
public class MongoDbCollection {
    private MongoDatabase database;
    
    /**
     * @param database 
     */
    public MongoDbCollection(MongoDatabase database) {
        this.database = database;
    }
    
    /**
     * @return MongoCollection
     */
    public MongoCollection<Document> getEvidence() {
        return this.database.getCollection("provas");
    }
}
