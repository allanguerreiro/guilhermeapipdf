/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.pdf.db.collections;

import com.api.pdf.db.MongoDbCollection;
import com.api.pdf.db.MongoDbConnection;
import com.mongodb.client.MongoCollection;
import static com.mongodb.client.model.Filters.eq;
import java.util.ArrayList;
import java.util.List;
import org.bson.Document;

/**
 *
 * @author guilherme
 */
public class FindAllStudentsProofs {
    /**
     * @param enrollment
     * @return 
     */
    public List<Document> getDocuments(int enrollment) {
        MongoDbCollection dbCollection = new MongoDbCollection(new MongoDbConnection().getDatabase());
        MongoCollection<Document> collection = dbCollection.getEvidence();
        
        if (enrollment != 0) {
            return collection.find(eq("enrollment", enrollment)).into(new ArrayList());
        }
        
        return collection.find().into(new ArrayList());
    }
}
