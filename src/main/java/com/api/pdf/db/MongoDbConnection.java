package com.api.pdf.db;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoDatabase;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */
public class MongoDbConnection {
    private MongoClient client;

    public MongoDbConnection() {
        try {
            MongoClientURI clientUri = new MongoClientURI("mongodb://192.168.10.135:27017");

            this.client = new MongoClient(clientUri);
        } catch (Exception e) {
            System.out.println("DB_ERRO: " + e);
        }
    }

    /**
     * @return MongoDatabase
     */
    public MongoDatabase getDatabase() {
        return this.client.getDatabase("multidisciplinar");
    }
}
