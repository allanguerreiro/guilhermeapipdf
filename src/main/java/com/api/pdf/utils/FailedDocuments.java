/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.pdf.utils;

/**
 *
 * @author guilherme
 */
public class FailedDocuments {
    private String path;
    private String documentName;
    
    public FailedDocuments(String path, String documentName) {
        this.path = path;
        this.documentName = documentName;
    }

    public String getPath() {
        return path;
    }

    public String getDocumentName() {
        return documentName;
    }
}
