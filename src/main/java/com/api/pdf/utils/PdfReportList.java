/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.pdf.utils;

import java.util.List;

/**
 *
 * @author guilherme
 */
public class PdfReportList {
    private List<CreatedDocuments> created;
    private List<FailedDocuments> failed;
    private int total;
    
    public PdfReportList(List<CreatedDocuments> created, List<FailedDocuments> failed, int total) {
        this.created = created;
        this.failed = failed;
        this.total = total;
    }

    public List<CreatedDocuments> getCreated() {
        return created;
    }

    public List<FailedDocuments> getFailed() {
        return failed;
    }

    public int getTotal() {
        return total;
    }
}
