/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.pdf.utils;

/**
 *
 * @author guilherme
 */
public class CreatedDocuments {
    private String path;
    private String filename;
    
    public CreatedDocuments(String path, String filename) {
        this.path = path;
        this.filename = filename;
    }

    public String getPath() {
        return path;
    }

    public String getFilename() {
        return filename;
    }
}
