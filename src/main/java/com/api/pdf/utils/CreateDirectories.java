/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.pdf.utils;

import java.io.File;
import org.bson.Document;

/**
 *
 * @author guilherme
 */
public class CreateDirectories {
    private Document document;
    
    /**
     * @param document 
     */
    public CreateDirectories(Document document) {
        this.document = document;
    }
    
    public File create() {
        String serverPath = System.getProperty("catalina.base") + "/webapps/";
        Document testSite = (Document) this.document.get("testSite");
        File basePath = new File(serverPath + this.document.getInteger("year") + "-" + this.document.getInteger("period"));
            
        if (! basePath.exists()) {
            basePath.mkdir();
        }

        File dirUnit  = new File(basePath.toString() + "/" + testSite.get("campus"));
        File dirShift = new File(dirUnit.toString() + "/" + testSite.get("shift"));
        File dirRoom  = new File(dirShift.toString() + "/" + testSite.get("room"));

        if (! dirUnit.exists()) {
            dirUnit.mkdir();
        }

        if (! dirShift.exists()) {
            dirShift.mkdir();
        }

        if (! dirRoom.exists()) {
            dirRoom.mkdir();
        }
        
        return dirRoom;
    }
}
